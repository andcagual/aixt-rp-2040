// Project Name: Aixt project, https://gitlab.com/fermarsan/aixt-project.git
// File Name: sleep_us.c
// Author: Fernando Martínez Santa
// Date: 2022-2023
// License: MIT
//
// Description: Milliseconds delay function
//              (Explorer16-PIC24 port)
#define sleep_ms(TIME)    __delay_ms(TIME)