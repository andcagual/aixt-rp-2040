// Project Name: Aixt project, https://gitlab.com/fermarsan/aixt-project.git
// File Name: time.c
// Author: Fernando Martínez Santa
// Date: 2022-2023
// License: MIT
//
// Description: Includes all the timing functions 
//              (Explorer16-PIC24 port)
#include "./time/sleep_us.c"
#include "./time/sleep_ms.c"