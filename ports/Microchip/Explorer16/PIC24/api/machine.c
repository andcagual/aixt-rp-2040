// Project Name: Aixt project, https://gitlab.com/fermarsan/aixt-project.git
// File Name: builtin.c
// Author: Fernando Martínez Santa
// Date: 2022-2023
// License: MIT
//
// Description: Includes all machine modules
//              (Explorer16-PIC24)
#include "./machine/pin.c"
// #include "./machine/uart.c"
