// Project Name: Aixt project, https://gitlab.com/fermarsan/aixt-project.git
// File Name: time.c
// Author: Fernando Martínez Santa
// Date: 2022-2023
// License: MIT
//
// Description: Includes all the timing functions 
//              (PC port). 
#include "./time/sleep_us.c"
#include "./time/sleep_ms.c"
#include "./time/sleep.c"