module equivalents

pub dict := {
	'x':	'X'
	'y':	'YY'
	'z':	'ZZZ'
}